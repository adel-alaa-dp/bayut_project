# Scrapy settings for bayut_scraper project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = "bayut_scraper"

SPIDER_MODULES = ["bayut_scraper.spiders"]
NEWSPIDER_MODULE = "bayut_scraper.spiders"


# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'bayut_scraper (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
# COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#    'bayut_scraper.middlewares.BayutScraperSpiderMiddleware': 543,
# }

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
# DOWNLOADER_MIDDLEWARES = {
#    'bayut_scraper.middlewares.BayutScraperDownloaderMiddleware': 543,
# }

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
# ITEM_PIPELINES = {
#    'bayut_scraper.pipelines.BayutScraperPipeline': 300,
# }

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
# AUTOTHROTTLE_ENABLED = True
# The initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
"""
Scrapy settings for the Bayut scraper.

This module contains the settings for the Bayut scraper, including the MongoDB connection URI,
the name of the database to use, and the names of the MongoDB collections for all and unique
listings. The module also sets the pipeline to use for storing scraped data in MongoDB, and
configures the download delay and request fingerprinter implementation.

Attributes:
    MONGO_URI: The URI for the MongoDB server.
    MONGO_DATABASE: The name of the MongoDB database to use.
    ALL_MONGO_COLLECTION: The name of the MongoDB collection for all listings.
    UNQ_MONGO_COLLECTION: The name of the MongoDB collection for unique listings.
    ITEM_PIPELINES: The Scrapy pipeline to use for storing scraped data in MongoDB.
    DOWNLOAD_DELAY: The number of seconds to wait between requests.
    REQUEST_FINGERPRINTER_IMPLEMENTATION: The implementation of the request fingerprinter.
"""
MONGO_URI = (
    "mongodb+srv://bayut-user:Qg7MooOTo1TYZtP0@bauyt-cluster.gudbzuy.mongodb.net/"
)
MONGO_DATABASE = "bauytDB"
ALL_MONGO_COLLECTION = "allListings"
UNQ_MONGO_COLLECTION = "uniqueListings"

ITEM_PIPELINES = {
    "bayut_scraper.pipelines.MongoDBPipeline": 300,
}

DOWNLOAD_DELAY = 2
REQUEST_FINGERPRINTER_IMPLEMENTATION = "2.7"
