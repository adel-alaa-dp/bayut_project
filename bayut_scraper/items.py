# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BayutScraperItem(scrapy.Item):
    """
    A Scrapy item for storing data about residential properties on Bayut.

    Attributes:
        num_rooms: The number of rooms in the property.
        pro_type: The type of property (e.g. apartment, villa, townhouse).
        area: The area of the property in square feet.
        location: The location of the property in Dubai.
        prop_id: A unique identifier for the property.
    """

    num_rooms = scrapy.Field()
    pro_type = scrapy.Field()
    area = scrapy.Field()
    location = scrapy.Field()
    prop_id = scrapy.Field()
