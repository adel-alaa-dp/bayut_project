import pymongo


class MongoDBPipeline(object):
    """
    A Scrapy pipeline for storing scraped data in a MongoDB database.

    This pipeline stores scraped property data in two MongoDB collections: one for all
    properties scraped so far, and one for unique properties (identified by their `prop_id`
    attribute). If a property is not already in the unique collection, it is added to both
    collections. If a property is already in the unique collection, it is only added to the
    all collection.

    Attributes:
        mongo_uri: The URI for the MongoDB server.
        mongo_db: The name of the MongoDB database to use.
        all_collection: The name of the MongoDB collection for all properties.
        unique_collection: The name of the MongoDB collection for unique properties.
    """

    def __init__(self, mongo_uri, mongo_db, ALL_MONGO_COLLECTION, UNQ_MONGO_COLLECTION):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.all_collection = ALL_MONGO_COLLECTION
        self.unique_collection = UNQ_MONGO_COLLECTION

    @classmethod
    def from_crawler(cls, crawler):
        """
        Creates and returns a new `MongoDBPipeline` instance using settings from the Scrapy
        crawler.

        Args:
            crawler: The Scrapy crawler instance.

        Returns:
            A new `MongoDBPipeline` instance.
        """
        return cls(
            mongo_uri=crawler.settings.get("MONGO_URI"),
            mongo_db=crawler.settings.get("MONGO_DATABASE"),
            ALL_MONGO_COLLECTION=crawler.settings.get("ALL_MONGO_COLLECTION"),
            UNQ_MONGO_COLLECTION=crawler.settings.get("UNQ_MONGO_COLLECTION"),
        )

    def open_spider(self, spider):
        """
        Initializes the MongoDB client and database.

        Args:
            spider: The Scrapy spider instance.
        """
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        """
        Closes the MongoDB client.

        Args:
            spider: The Scrapy spider instance.
        """
        self.client.close()

    def process_item(self, item, spider):
        """
        Processes a scraped item and stores it in the appropriate MongoDB collections.

        Args:
            item: The Scrapy item to process.
            spider: The Scrapy spider instance.

        Returns:
            The processed item.
        """
        if self.db[self.unique_collection].find_one({"prop_id": item["prop_id"]}):
            self.db[self.all_collection].insert_one(dict(item))
        else:
            self.db[self.all_collection].insert_one(dict(item))
            self.db[self.unique_collection].insert_one(dict(item))
        return item
