import scrapy
from bayut_scraper.items import BayutScraperItem
import hashlib


class BayutSpider(scrapy.Spider):
    """
    A web scraper for Bayut, a real estate website in Dubai.

    This spider sends HTTP requests to the Bayut API to retrieve information about
    residential properties for sale and for rent in Dubai. The spider extracts
    relevant property data from the API responses and stores it in a `BayutScraperItem`
    object.

    Attributes:
        name: The name of the spider.
        allowed_domains: A list of domains that the spider is allowed to scrape.
        headers: A dictionary of HTTP headers to include with each request.
        data: A string containing the payload for each request to the Bayut API.
        api_url: The URL of the Bayut API endpoint.
    """

    name = "bayut"
    allowed_domains = ["bayut.com", "ll8iz711cs-dsn.algolia.net"]
    headers = {
        "Accept-Language": "en-US,en;q=0.9,ar;q=0.8",
        "Connection": "keep-alive",
        "Origin": "https://www.bayut.com",
        "Referer": "https://www.bayut.com/",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "cross-site",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36",
        "accept": "application/json",
        "content-type": "application/x-www-form-urlencoded",
        "sec-ch-ua": '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"Windows"',
    }
    data = '{"requests":[{"indexName":"bayut-production-ads-en","params":"page=0&hitsPerPage=24&query=&optionalWords=&facets=%5B%5D&maxValuesPerFacet=10&attributesToHighlight=%5B%5D&attributesToRetrieve=%5B%22agency%22%2C%22area%22%2C%22baths%22%2C%22category%22%2C%22contactName%22%2C%22externalID%22%2C%22id%22%2C%22location%22%2C%22objectID%22%2C%22phoneNumber%22%2C%22coverPhoto%22%2C%22photoCount%22%2C%22price%22%2C%22product%22%2C%22productLabel%22%2C%22purpose%22%2C%22geography%22%2C%22permitNumber%22%2C%22referenceNumber%22%2C%22rentFrequency%22%2C%22rooms%22%2C%22slug%22%2C%22slug_l1%22%2C%22slug_l2%22%2C%22title%22%2C%22title_l1%22%2C%22title_l2%22%2C%22createdAt%22%2C%22updatedAt%22%2C%22ownerID%22%2C%22isVerified%22%2C%22propertyTour%22%2C%22verification%22%2C%22completionStatus%22%2C%22furnishingStatus%22%2C%22-agency.tier%22%2C%22requiresLogin%22%2C%22coverVideo%22%2C%22videoCount%22%2C%22description%22%2C%22description_l1%22%2C%22description_l2%22%2C%22floorPlanID%22%2C%22panoramaCount%22%2C%22hasMatchingFloorPlans%22%2C%22hasTransactionHistory%22%2C%22state%22%2C%22photoIDs%22%2C%22reactivatedAt%22%2C%22hidePrice%22%2C%22extraFields%22%2C%22projectNumber%22%2C%22locationPurposeTier%22%2C%22ownerAgent%22%5D&filters=purpose%3A%22NONE%22%20AND%20category.slug%3A%22residential%22%20AND%20(location.slug%3A%22%2Fdubai%22)&numericFilters="}]}'
    api_url = "https://ll8iz711cs-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)&x-algolia-application-id=LL8IZ711CS&x-algolia-api-key=802406b04be9b83e3a59dbb7e61e2778"

    def start_requests(self):
        """
        Generates HTTP requests to the Bayut API for each purpose in the purposes list.

        Yields:
            A `scrapy.Request` object for each purpose in the purposes list.

        Raises:
            ValueError: If the data string contains the string "NONE".
        """
        purposes_list = ["for-sale", "for-rent"]
        for purpose in purposes_list:
            formatted_data = self.data.replace("NONE", purpose)
            yield scrapy.Request(
                self.api_url,
                method="POST",
                body=str(formatted_data),
                headers=self.headers,
                callback=self.parse,
                meta={"payload": formatted_data},
            )

    def parse(self, response):
        """
        Parses the JSON response from the Bayut API and extracts property data.

        Args:
            response: The HTTP response from the Bayut API.

        Yields:
            A `BayutScraperItem` object for each property in the response.

            Generates HTTP requests to the Bayut API for each page of search results.

        Raises:
            ValueError: If the response JSON does not have the expected format.
        """
        response_json = response.json()
        total_pages = int(response_json["results"][0]["nbPages"])
        result_json = response.json()
        for res_ in result_json["results"][0]["hits"]:
            item = BayutScraperItem()
            item["num_rooms"] = res_["rooms"]
            item["pro_type"] = res_["category"][1]["name"]
            item["area"] = res_["area"]
            item["location"] = ", ".join(loc["name_l2"] for loc in res_["location"])
            item["prop_id"] = hashlib.sha256(
                "/".join(str(value) for value in item.values()).encode()
            ).hexdigest()
            yield item

        for page in range(1, total_pages + 1):
            paginated_data = response.meta["payload"].replace(
                "page=0&", f"page={page}&"
            )
            yield scrapy.Request(
                self.api_url,
                method="POST",
                body=str(paginated_data),
                headers=self.headers,
                callback=self.parse_page,
            )

    def parse_page(self, response):
        """
        Parses the JSON response from a paginated Bayut API request and extracts property data.

        Args:
            response: The HTTP response from the Bayut API.

        Yields:
            A `BayutScraperItem` object for each property in the response.

        Raises:
            ValueError: If the response JSON does not have the expected format.
        """
        result_json = response.json()
        for res_ in result_json["results"][0]["hits"]:
            item = BayutScraperItem()
            item["num_rooms"] = res_["rooms"]
            item["pro_type"] = res_["category"][1]["name"]
            item["area"] = res_["area"]
            item["location"] = ", ".join(loc["name_l2"] for loc in res_["location"])
            item["prop_id"] = hashlib.sha256(
                "/".join(str(value) for value in item.values()).encode()
            ).hexdigest()
            yield item
