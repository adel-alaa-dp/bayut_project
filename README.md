## Bayut Scraper
This is a Scrapy spider that scrapes property listings from Bayut, a popular real estate website in Dubai.

# Setup
To run the spider, you'll need to have Python 3.7 or higher installed, as well as the Scrapy package and the pymongo package.

You can install Scrapy and pymongo using pip:

```
pip install scrapy pymongo
```

# Running the Spider
To run the spider, navigate to the project directory in your terminal and run:

```
scrapy crawl bayut
```
The spider will scrape property listings on Bayut (for-sale & for-rent).

The scraped data will be stored in your MongoDB database in two collections: 
 * one for all properties scraped so far (duplication possibility), and one for unique properties (identified by their prop_id attribute).

# Project Structure
The project contains the following files and directories:

* README.md: This file.
* bayut_scraper/: The Python package containing the spider and pipelines.
  * pipelines.py: The Scrapy pipeline for storing scraped data in MongoDB.
  * settings.py: The Scrapy settings file.
  * spiders/: The directory containing the spider.
   * bayut_spider.py: The Scrapy spider that scrapes property listings from Bayut.
* scrapy.cfg: The Scrapy configuration file.

# Contributing
Contributions to this project are welcome! If you find a bug or have a suggestion for improvement, please open an issue or submit a merge request.

# License
This project is licensed under the MIT License - see the LICENSE file for details.
